#ifndef FCFS_H
#define FCFS_H
#include<iostream>
#include<stdlib.h>
#include<iomanip>
#include<stdio.h>
using namespace std;


class FCFS
{
    public:
        void insertar(int cantProcesos,int aleatorio);
        void fcfs(int cantProcesos);
};

#endif // FCFS_H
