#include "HRN.h"

struct procesoHrn{
    double proceso,llegada,timeExe1,timeIO,timeExe2,completado1,completado2,llegada2,
           tiempoServicio,tiempoEspera,indiceServicio,tiempoEjecucion;
}procesoshrn[10];
int cantProcesos1, aleatorio1;

void HRN::hrn(int cantProcesos1){
    struct procesoHrn temp;
    for(int i=0;i<cantProcesos1-1;i++){
        for(int j=i+1;j<cantProcesos1;j++){
            if(procesoshrn[i].llegada>procesoshrn[j].llegada){
                    temp=procesoshrn[i];
                    procesoshrn[i]=procesoshrn[j];
                    procesoshrn[j]=temp;
                }
        }
    }
}


void HRN::insertar(int cantProcesos1,int aleatorio1){
    double tPromFinal,tPromServicio,tPromEspera,indPromServicios;
    int tiempofinal = procesoshrn[0].llegada;
    if(aleatorio1==1){
        for(int i=0;i<cantProcesos1;i++){

                    procesoshrn[i].proceso= i+1;
                    procesoshrn[i].llegada = rand()%29;
                    procesoshrn[i].timeExe1 = rand()%29+1;
                    procesoshrn[i].timeIO = rand()%29+1;
                    procesoshrn[i].timeExe2 = rand()%29+1;

                    tiempofinal = 0;
                    tPromFinal = 0;
                    tPromServicio = 0;
                    tPromEspera = 0;
                    indPromServicios = 0;

                    procesoshrn[i].completado1 = 0;
                    procesoshrn[i].completado2 = 0;
                    procesoshrn[i].llegada2 = 99999;
                }

    }else if(aleatorio1==2){
        for(int i=0;i<cantProcesos1;i++){
                    procesoshrn[i].proceso=i+1;
                    cout<<"Proceso "<< i+1 <<endl;
                    cout<<"Ingrese el tiempo de llegada : ";
                    cin>>procesoshrn[i].llegada;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas 1 de CPU: ";
                    cin>>procesoshrn[i].timeExe1;
                    cout<<"Ingrese el tiempo de ejecucion de las rafagas de E/S : ";
                    cin>>procesoshrn[i].timeIO;
                    cout<<"Ingrese el tiempo de ejecucion de la rafaga 2 de CPU: ";
                    cin>>procesoshrn[i].timeExe2;

                    tiempofinal = 0;
                    tPromFinal = 0;
                    tPromServicio = 0;
                    tPromEspera = 0;
                    indPromServicios = 0;

                    procesoshrn[i].completado1 = 0;
                    procesoshrn[i].completado2 = 0;
                    procesoshrn[i].llegada2 = 99999;
                }
    }
    hrn(cantProcesos1);
    bool stop = false;

    cout<<"\n\t\t Algoritmo de Planificacion HRN: HIGHEST REST TIME NEXT"<<endl<<endl;
	cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
    cout<<"|  Proceso  | T. Llegada | T. Ejecucion |    E/S    | T. Ejecucion | T. Final | T. Servicio | T. Espera | I. Servicio |"<<endl;

    do
    {
        int aux = 0;
        float hrr = -99999;
        float temp = 0;
        int loc;
        for(int i=0;i<cantProcesos1;i++){
            if((procesoshrn[i].completado1==0)  && (procesoshrn[i].llegada <= tiempofinal)){
                temp = (procesoshrn[i].timeExe1 + (tiempofinal-procesoshrn[i].llegada))/procesoshrn[i].timeExe1;
                if(hrr < temp){
                    hrr = temp;
                    loc = i;
                    aux = 1;
                }
            }

            else if((procesoshrn[i].completado2==0)  && (procesoshrn[i].llegada2 <= tiempofinal)){
                temp = (procesoshrn[i].timeExe2 + (tiempofinal-procesoshrn[i].llegada2))/procesoshrn[i].timeExe2;
                if(hrr < temp){
                    hrr = temp;
                    loc = i;
                    aux = 2;
                }
            }
            else if(aleatorio1==1 && i==(cantProcesos1-1)){
                tiempofinal += 1;
            }
        }

        if(aux == 1){
            tiempofinal += procesoshrn[loc].timeExe1;
            procesoshrn[loc].completado1 = 1;
            procesoshrn[loc].llegada2 = (tiempofinal + procesoshrn[loc].timeIO);
        }

        else if(aux == 2){
            tiempofinal += procesoshrn[loc].timeExe2;
            procesoshrn[loc].completado2 = 1;
            procesoshrn[loc].tiempoServicio = (tiempofinal - procesoshrn[loc].llegada);
            procesoshrn[loc].tiempoEspera = (procesoshrn[loc].tiempoServicio - (procesoshrn[loc].timeExe1 + procesoshrn[loc].timeExe2));
            procesoshrn[loc].indiceServicio = ((procesoshrn[loc].timeExe1 + procesoshrn[loc].timeExe2) /procesoshrn[loc].tiempoServicio);

            tPromFinal += tiempofinal;
            tPromServicio += procesoshrn[loc].tiempoServicio;
            tPromEspera += procesoshrn[loc].tiempoEspera;
            indPromServicios += procesoshrn[loc].indiceServicio;

            cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
            cout<<"|     "<<setprecision(0)<<procesoshrn[loc].proceso<<"     |     "
                          <<procesoshrn[loc].llegada<<"      |      "
                          <<procesoshrn[loc].timeExe1<<"       |     "
                          <<procesoshrn[loc].timeIO<<"     |      "
                          <<procesoshrn[loc].timeExe2<<"       |    "
                          <<tiempofinal<<"    |     "
                          <<procesoshrn[loc].tiempoServicio<<"      |    "
                          <<procesoshrn[loc].tiempoEspera<<"     |     "
                          <<setprecision(2)<<procesoshrn[loc].indiceServicio<<"     |"<<endl;
        }

        for(int i=0;i<cantProcesos1;i++){
            stop = true;
            if(procesoshrn[i].completado2 == 0){
                stop = false;
                break;
            }
        }
    }while(!stop);

    tPromFinal /= cantProcesos1;
    tPromServicio /= cantProcesos1;
    tPromEspera /= cantProcesos1;
    indPromServicios /= cantProcesos1;

    cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
    cout<<"|-----------|------------|--------------|-----------|--------------|  Tiempo  |    Tiempo   |  Tiempo   |   Indice    |"<<endl;
    cout<<"|-----------|------------|--------------|-----------|--------------| Promedio |   Promedio  | Promedio  |   Promedio  |"<<endl;
    cout<<"|           |            |              |           |              |   Final  | de Servicio | de Espera | de Servicio |"<<endl;
    cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
    cout<<"|           |            |              |           |              |   "<<fixed << setprecision(2) <<tPromFinal<<"  |     "<<tPromServicio<<"   |   "<<tPromEspera<<"   |    "<<indPromServicios<<"     |"<<endl;
    cout<<"|-----------|------------|--------------|-----------|--------------|----------|-------------|-----------|-------------|"<<endl;
}
