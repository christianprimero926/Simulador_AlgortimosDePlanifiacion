#include <iostream>
#include<stdlib.h>
#include<stdio.h>
#include"FCFS.h"
#include"HRN.h"
#include"SRT.h"
#include"RR.h"

using namespace std;

void menu(){
        cout<<"\n\n\t\tSimulador De Algoritmos de Planificacion" <<endl<<endl;
        cout<<"Seleccione el algoritmo de Planificacion que desea utilizar:"<<endl<<endl;
        cout<<"1.- FCFS (Primero en llegar Primero en Salir)"<<endl;
        cout<<"2.- HRN (Tasa de respuesta mas corta)"<<endl;
        cout<<"3.- SRT (Tiempo restante mas corto)"<<endl;
        cout<<"4.- RR (Round-Robin)"<<endl;
        cout<<"5.- Salir"<<endl<<endl;
        cout<<"Digite una opcion: ";
}
void goBack(){
        cout<<"\nQue desea hacer :"<<endl;
        cout<<"1.- Volver al menu."<<endl;
        cout<<"2.- Salir."<<endl<<endl;
}
void random(){
    cout<<"\nComo desea ingresar los datos :"<<endl;
    cout<<"1.- De manera Aleatoria."<<endl;
    cout<<"2.- Por Consola."<<endl;
    cout<<"3.- Salir."<<endl<<endl;
}


int main(){
    FCFS procesofcfs;
    HRN procesohrn;
    SRT procesosrt;
    RR procesorr;

    int volver=0,opcion,aleatorio;
    int cantProcesos,llegada,timeExe1,timeIO,timeExe2,quantum,prioridad;

    do{
        menu();
        cin>>opcion;
        cout<<endl;

        switch(opcion){
        case 1:
            cout<<"\t Algoritmo de Planificacion FCFS: FIRST COME FIRST SERVERED"<<endl<<endl;
            do{
                cout<<"ingresar cantidad procesos: ";
                cin>>cantProcesos;
            }while (cantProcesos <= 0 || cantProcesos > 10);

            random();
            do{
                cout<<"Digite una opcion: ";
                cin>>aleatorio;
                switch(aleatorio){
                        case 1:
                            procesofcfs.insertar(cantProcesos,aleatorio);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 2:
                            procesofcfs.insertar(cantProcesos,aleatorio);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 3:
                            return 0;
                            break;

                        default:
                            cout<< "Opci�n no valida\n";
                }
            }while(!(volver>=1 and volver<=3));
        break;

        case 2:
            cout<<"\t Algoritmo de Planificacion HRN: HIGHEST REST TIME NEXT"<<endl<<endl;
            do{
                cout<<"ingresar cantidad procesos: ";
                cin>>cantProcesos;
            }while (cantProcesos <= 0 || cantProcesos > 10);
            random();
            do{
                cout<<"Digite una opcion: ";
                cin>>aleatorio;
                switch(aleatorio){
                        case 1:
                            procesohrn.insertar(cantProcesos,aleatorio);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 2:
                            procesohrn.insertar(cantProcesos,aleatorio);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 3:
                            return 0;
                            break;

                        default:
                            cout<< "Opci�n no valida\n";
                }
            }while(!(volver>=1 and volver<=3));
            break;

        case 3:
            cout<<"\t Algoritmo de Planificacion SRT: SHORTEST REMAINING TIME"<<endl<<endl;
            do{
                cout<<"ingresar cantidad procesos: ";
                cin>>cantProcesos;
            }while (cantProcesos <= 0 || cantProcesos > 10);
            random();
            do{
                cout<<"Digite una opcion: ";
                cin>>aleatorio;
                switch(aleatorio){
                        case 1:
                            procesosrt.insertar(cantProcesos,aleatorio);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 2:
                            procesosrt.insertar(cantProcesos,aleatorio);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 3:
                            return 0;
                            break;

                        default:
                            cout<< "Opci�n no valida\n";
                }
            }while(!(volver>=1 and volver<=3));
            break;

        case 4:
            cout<<"\t Algoritmo de Planificacion RR: ROUND ROBIN"<<endl<<endl;
            do{
                cout<<"ingresar cantidad procesos: ";
                cin>>cantProcesos;
                cout<<"ingresar quantum: ";
                cin>>quantum;
            }while (cantProcesos <= 0 || cantProcesos > 10);

            random();
            do{
                cout<<"Digite una opcion: ";
                cin>>aleatorio;
                switch(aleatorio){
                        case 1:
                            procesorr.insertar(cantProcesos,aleatorio,quantum);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 2:
                            procesorr.insertar(cantProcesos,aleatorio,quantum);
                            goBack();
                            do{
                                cout<<"Digite una opcion: ";
                                cin>>volver;
                                switch(volver){
                                        case 1:
                                            system("cls");
                                            main();
                                        case 2:
                                            return 0;

                                        default:
                                            cout<< "Opci�n no valida\n";

                               }
                            }while(!(volver>=1 and volver<=2));
                            break;
                        case 3:
                            return 0;
                            break;

                        default:
                            cout<< "Opci�n no valida\n";
                }
            }while(!(volver>=1 and volver<=3));

            break;
        case 5:
            return 0;

        default:
            cout<< "Opci�n no valida\n";
            system("pause");
            system("cls");
            break;
        }

    }while(!(opcion>=1 and opcion<=5));





    system("cls");


    return 0;
}

